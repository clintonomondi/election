alertify.set('notifier','position', 'top-center');
function refreshPage(){
    window.location.reload();
}

function changeElectionStatus(id,status){
    $('body').preloader({text: 'Please Wait...!', });
    var data;
    data = new FormData();
    data.append('id', id);
    data.append('status', status);
    $.ajax({
        url: "/elections/updateStatus",
        method: "POST",
        contentType: false,
        processData: false,
        catche: false,
        data: data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $('body').preloader('remove');
            if(data.status){
                refreshPage();
            }

        },
        error:function(data) {
            $('body').preloader('remove');
        }
    });

}

function Store(user_id,election_id,posts_id,candidate_id,value) {
    $('body').preloader({text: 'Please Wait...!', });

    if(value>0){
        $('body').preloader('remove');
      alertify.error('Sorry, You have already voted.')
    }else {
        var data;
        data = new FormData();
        data.append('user_id', user_id);
        data.append('election_id', election_id);
        data.append('posts_id', posts_id);
        data.append('candidate_id', candidate_id);
        $.ajax({
            url: "/election/postTemp",
            method: "POST",
            contentType: false,
            processData: false,
            catche: false,
            data: data,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $('body').preloader('remove');
                if (data.status) {
                    unselect();
                    $.each(data.data, function (i, item) {
                        document.getElementById("cand" + item.candidate_id).style.border = "dotted #ed4266";
                        document.getElementById("checker" + item.candidate_id).checked = true;
                    });

                }

            },
            error: function (data) {
                $('body').preloader('remove');
            }
        });
    }
}

function unselect() {
    var x=document.getElementsByClassName("single-team-box");
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].style.border  = "none";
    }

    var c=document.getElementsByClassName("checker");
    var j;
    for (j = 0; j < x.length; j++) {
        c[j].checked = false;
    }
}
