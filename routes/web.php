<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Election;

Route::get('/', function () {
    $elections=Election::where('status','Active')->get();
    return view('welcome',compact('elections'));
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//election
Route::get('/election', 'ElectionController@index')->name('election');
Route::post('/election', 'ElectionController@postElection')->name('postElection');
Route::get('/elections', 'ElectionController@elections')->name('elections');
Route::get('/elections/edit/{id}', 'ElectionController@edit')->name('election/edit');
Route::post('/elections/updateStatus', 'ElectionController@updateStatus')->name('election/updateStatus');
Route::post('/savePosts/{id}', 'ElectionController@savePosts')->name('savePosts');
Route::post('/updateElection/{id}', 'ElectionController@updateElection')->name('updateElection');
Route::get('/election/info/{id}', 'ElectionController@info')->name('election/info');
Route::get('/election/leaders/{id}', 'ElectionController@leaders')->name('election/leaders');
Route::get('/election/elect/{id}', 'ElectionController@elect')->name('election/elect');
Route::post('/election/postTemp', 'ElectionController@postTemp')->name('election/postTemp');
Route::get('/election/leader/{id}', 'ElectionController@leader')->name('election/leader');
Route::post('/postElected/{id}', 'ElectionController@postElected')->name('postElected');

//candidate
Route::get('/election/candidates/{id}', 'CandidateController@index')->name('candidate/index');
Route::get('/election/apply/{id}', 'CandidateController@apply')->name('candidate/apply');
Route::post('/election/postApllication/{id}', 'CandidateController@postApllication')->name('postApllication');



//POSTS
Route::get('/election/posts/{id}', 'PostController@index')->name('election/posts');
Route::get('/posts/leaders/{id}', 'PostController@leaders')->name('posts/leaders');
