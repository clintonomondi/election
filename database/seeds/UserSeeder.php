<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                DB::table('users')->insert([
            'name' => 'Election Admin',
            'email' => 'admin'.'@gmail.com',
            'role'=>'admin',
            'phone'=>'0712083128',
            'password' => bcrypt('admin'),
        ]);
    }
}
