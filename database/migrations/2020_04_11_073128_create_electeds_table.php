<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateElectedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id');
            $table->string('election_id');
            $table->string('posts_id');
            $table->string('candidate_id');
//           $table->unique(array('user_id', 'election_id','posts_id'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electeds');
    }
}
