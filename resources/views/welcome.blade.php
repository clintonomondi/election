@extends('layouts.common')

@section('content')

    <!-- Start Main Banner Area -->
    <div class="home-slides owl-carousel owl-theme">
        <div class="main-banner item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="banner-content">
                            <h1>Help Bring The Change We Need</h1>
                            <p>It is enough that the people know there was an election. The people who cast the votes decide nothing. The people who count the votes decide everything.</p>

                            <a href="{{route('login')}}" class="default-btn">LOGIN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner item-bg2 jarallax" data-jarallax='{"speed": 0.3}'>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="banner-content">
                            <h1>Help Bring The Change We Need</h1>
                            <p>It is enough that the people know there was an election. The people who cast the votes decide nothing. The people who count the votes decide everything.</p>

                            <a href="#" class="default-btn">Join With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner item-bg3 jarallax" data-jarallax='{"speed": 0.3}'>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="banner-content">
                            <h1>Help Bring The Change We Need</h1>
                            <p>It is enough that the people know there was an election. The people who cast the votes decide nothing. The people who count the votes decide everything.</p>

                            <a href="#" class="default-btn">Join With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Banner Area -->



    <!-- Start Boxes Area -->
    <section class="boxes-area">
        <div class="container">
            <div class="row justify-content-center">
                @foreach($elections as $election)
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-box">
                            <div class="icon">
                                <i class="flaticon-tickets"></i>
                            </div>

                            <h3>{{$election->title}}</h3>

                            <p>{{$election->description}}.</p>
                            <h6>Start date  {{$election->start_date}}</h6>
                            <h6>End date  {{$election->end_date}}</h6>
                            <div hidden>
                                {{$date = strtotime($election->end_date)}}
                                {{$remaining = $date - time()}}

                                {{$days_remaining = floor($remaining / 86400)}}
                                {{$hours_remaining = floor(($remaining % 86400) / 3600)}}
                                {{$word="$days_remaining days and $hours_remaining hours left"}}
                            </div>

                            <p>{{$word}}</p>

                            <a href="{{route('login')}}" class="details-btn"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    <!-- Start About Area -->
{{--    <section class="about-area ptb-100">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6 col-md-12 col-sm-12">--}}
{{--                    <div class="about-image">--}}
{{--                        <img src="assets/img/about-image1.jpg" alt="image">--}}

{{--                        <div class="text">--}}
{{--                            More than <span>20</span> years of Leadership--}}
{{--                        </div>--}}

{{--                        <div class="shape">--}}
{{--                            <img src="assets/img/dot.png" alt="image">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-lg-6 col-md-12 col-sm-12">--}}
{{--                    <div class="about-content">--}}
{{--                        <span class="sub-title">About Politician</span>--}}
{{--                        <h2>Meet Albert, The Next Country Leader</h2>--}}
{{--                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt  labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>--}}

{{--                        <div class="signature-image">--}}
{{--                            <img src="assets/img/signature-img.png" alt="image">--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
    <!-- End About Area -->
@endsection
