@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="single-box2">
                <div class="icon">
                    <i class="flaticon-tickets"></i>
                </div>

                <h3>Elections</h3>
                <table class="table table-bordered table-striped table-hover">
                    <thead>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Nomination start Date</th>
                    <th>Nomination end Date</th>
                    <th>Status</th>
                    <th></th>
                    <th></th>
                    </thead>
                    <tbody>
                    @foreach($elections as $key=>$election)
                        <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$election->title}}</td>
                        <td>{{$election->description}}</td>
                        <td>{{$election->start_date}}</td>
                        <td>{{$election->end_date}}</td>
                        <td>{{$election->nom_start_date}}</td>
                        <td>{{$election->nom_end_date}}</td>
                            @if($election->status=='Active')
                            <td class="text-success">{{$election->status}}</td>
                                @else
                                <td class="text-danger">{{$election->status}} </td>
                        @endif
                       <td><a class="btn btn-primary btn-sm" href="{{route('election/edit',$election->id)}}"><span class="fa fa-edit">Edit</span> </a> </td>
                       <td><a class="btn btn-info btn-sm" href="{{route('election/info',$election->id)}}"><span class="fa fa-info">Info</span> </a> </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>

@endsection
