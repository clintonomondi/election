@extends('layouts.app')

@section('content')
    <!-- Start History Area -->
    <section class="history-area">
        <div class="container-fluid">
            <div class="timeline history-timeline">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($election->posts as $key=>$post)
                        <div class="swiper-slide bg{{$key+1}}" data-year="{{$key+1}}">
                            <div class="swiper-slide-content">
                                <span class="timeline-year">{{$post->title}}</span>
                                <h3 class="timeline-title">Results</h3>
                                <p class="timeline-text">
                                    <div class="row">
                                       @foreach($post->candidate as $candidate)
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-6">
                                            <div class="single-funfact">
                                                <h3><span class="odometer" data-count="{{\App\Elected::where('candidate_id',$candidate->id)->where('election_id',$election->id)->count()}}"></span><span class="sign-icon"></span></h3>
                                        <p>{{$candidate->user->name}}</p>
                                        </div>
                                          </div>
                                       @endforeach

                                </div>

                                </p>
                            </div>
                        </div>
                        @endforeach

                    </div>

                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </section>

    <!-- Start Join With Us Area -->
    <section class="join-with-us-area">
        <div class="container">
            <div class="join-with-us-content">
                <span class="sub-title">{{$election->title}}</span>

                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-box">
                            <div class="icon">
                                <i class="flaticon-money"></i>
                            </div>

                            <h3>Nominated Candidates</h3>

                            <p>These are candidates who will be voted.</p>
                            <p><strong>Applied:</strong>    {{$election->candidate->count()}}</p>
                            <p><strong>Nominated:</strong>  {{$election->candidate->count()}}</p>

                            <a href="{{route('candidate/index',$election->id)}}" class="details-btn"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-box">
                            <div class="icon">
                                <i class="flaticon-tax"></i>
                            </div>

                            <h3>Available Posts</h3>

                            <p>Nomination is open for this post until <span>{{$election->nom_end_date}}</span>.</p>
                            <h5>{{$election->posts->count()}}</h5>

                            <a href="{{route('election/posts',$election->id)}}" class="details-btn"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-0 offset-md-3 offset-sm-3">
                        <div class="single-box">
                            <div class="icon">
                                <i class="flaticon-university"></i>
                            </div>

                            <h3>Elect Leaders</h3>

                            <p>Lorem ipsum dolor sit consectetua dipiscing elit sed do eiusmod.</p>

                            <a href="{{route('election/elect',$election->id)}}" class="details-btn"><i class="flaticon-right-arrow"></i></a>
                        </div>
                    </div>
                </div>


                <div class="shape"><img src="{{asset('assets/img/dot-shape.png')}}" alt="image"></div>
            </div>
        </div>
    </section>
    <!-- End Join With Us Area -->

@endsection
