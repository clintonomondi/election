@extends('layouts.app')

@section('content')

    <!-- Start About Area -->
    @foreach($election->candidate as $key=>$candidate)
    <section class="about-area ptb-90">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="about-image">
                        <img src="/storage/avatars/{{$candidate->avatar}}" alt="image">

                        <div class="text">
                         <span>{{$candidate->posts->title}}</span>
                        </div>

                        <div class="shape">
                            <img src="{{asset('assets/img/dot.png')}}" alt="image">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="about-content">
                        <span class="sub-title">{{$candidate->user->name}}</span>
                        <h2> {{$candidate->title}}</h2>
                        <p>{!!str_limit($candidate->portfolio,300)!!} <a href="{{route('election/leader',$candidate->user_id)}}" style="color: #ed4266">read more</a></p>

                        <div class="signature-image">
                            <img src="{{asset('assets/img/signature-img.png')}}" alt="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><hr>
    @endforeach
    <!-- End About Area -->

@endsection
