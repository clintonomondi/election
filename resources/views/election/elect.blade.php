@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="single-box2">
                @if($data>0)
                    <div class="alert alert-danger">
                        <button class="close" data-dismiss="alert">&times;</button>
                        <h4>Warning!</h4>
                        <p>Sorry, you cannot elect any leader in this election.</p>
                        <p>They have   already voted in this election.</p>
                    </div>
                    @endif
                    @include('includes.message')
{{--                <div class="icon">--}}
{{--                    <i class="flaticon-tickets"></i>--}}
{{--                </div>--}}

{{--                <h3>Elect leaders</h3>--}}


    <form id="regForm" method="post" action="{{route('postElected',$election->id)}}">
        @csrf
        <h5>Elect Leaders:</h5>
        <!-- One "tab" for each step in the form: -->
        @foreach($election->posts as $key=>$post)
        <div class="tab">{{$post->title}}:
            <div class="row justify-content-center">
                @foreach($post->candidate as $key2=>$candit)
                <div class="col-sm-3">
                    <div class="single-team-box" id="cand{{$candit->id}}">
                        <div class="image">
                            <img src="/storage/avatars/{{$candit->avatar}}" alt="image">

                            <ul class="social">
                                <li><a href="http://facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="https://instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="https://linkedin.com/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>

                        <div class="content">
                            <h3>{{$candit->user->name}}</h3>
                            <span>{{$post->title}}</span>

                            <label class="checkbox">
                                <input type="checkbox" class="checker" id="checker{{$candit->id}}" onclick="Store('{{Auth::user()->id}}','{{$election->id}}','{{$post->id}}','{{$candit->id}}','{{$data}}')" />
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
                    @endforeach

            </div>
            <h6>{{$post->title}}</h6>
            @include('includes.message')
        </div>
        @endforeach
            <hr>
{{--        <div class="tab">Contact Info:--}}
{{--            <p><input placeholder="E-mail..." oninput="this.className = ''" name="email"></p>--}}
{{--            <p><input placeholder="Phone..." oninput="this.className = ''" name="phone"></p>--}}
{{--        </div>--}}
{{--        <div class="tab">Birthday:--}}
{{--            <p><input placeholder="dd" oninput="this.className = ''" name="dd"></p>--}}
{{--            <p><input placeholder="mm" oninput="this.className = ''" name="nn"></p>--}}
{{--            <p><input placeholder="yyyy" oninput="this.className = ''" name="yyyy"></p>--}}
{{--        </div>--}}
{{--        <div class="tab">Login Info:--}}
{{--            <p><input placeholder="Username..." oninput="this.className = ''" name="uname"></p>--}}
{{--            <p><input placeholder="Password..." oninput="this.className = ''" name="pword" type="password"></p>--}}
{{--        </div><hr>--}}
        @if($data==0)
        <div style="overflow:auto;">
            <div style="float:right;">
                <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
            </div>
        </div>
        @endif
        <!-- Circles which indicates the steps of the form: -->
        <div style="text-align:center;margin-top:40px;">
            @foreach($election->posts as $key=>$post)
            <span class="step"></span>
                @endforeach

        </div>
    </form>

            </div>
        </div>

    </div>


@endsection
