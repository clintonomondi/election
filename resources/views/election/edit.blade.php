@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="single-box2">
                <h3> Election Posts
               <span class="float-lg-right"> <a href="#" data-toggle="modal" data-target="#editelection" class="fa fa-edit">Edit</a></span>
                    @include('modals.editelection')
                </h3>
                <hr>
                @include('includes.message')
                <div class="row">
                    <div class="col-sm-6">

                <table class="table table-bordered">
                    <thead>
                    <td>#</td>
                    <td>Title</td>
                    <td>Description</td>
                    <td></td>
                    </thead>
                    <tbody>
                    @foreach($election->posts as $key=>$data)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$data->title}}</td>
                            <td>{{$data->description}}</td>
                            <td><a class="fa fa-edit" href="#"></a> </td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>

                    </div>
                    <div class="col-sm-6">
                       <div class="row justify-content-center">
                           <div class="col-sm-4">
                           <label>Election status</label>
                           </div>
                           <div class="col-sm-4">
                               @if($election->status=='Active')
                           <input  type="checkbox" checked onchange="changeElectionStatus('{{$election->id}}','Inactive')"  data-toggle="toggle" data-on="{{$election->status}}" data-off="Inactive" data-onstyle="success" data-offstyle="danger">
                          @else
                                   <input  type="checkbox" checked onchange="changeElectionStatus('{{$election->id}}','Active')"   data-toggle="toggle" data-on="{{$election->status}}" data-off="Active" data-onstyle="danger" data-offstyle="success">
                           @endif
                           </div>
                           <div class="col-sm-4">
                               <a class="btn btn-info " onclick="addPostDiv()"><span class="fa fa-address-card"></span>Add Post</a>
                           </div>
                       </div><hr>
                        <div class="contact-form">
                            <form  method="post" action="{{route('savePosts',$election->id)}}">
                                @csrf
                                <div id="postdiv">


                                </div>
<hr>
                                <div class="col-lg-12 col-md-12">
                                    @if($election->status=='Active')
                                    <button type="submit" class="default-btn">SAVE POSTS <span></span></button>
                                    @else
                                        <button DISABLED type="submit" class="default-btn">Inactive <span></span></button>
                                        @endif
                                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
