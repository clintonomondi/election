@extends('layouts.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="single-box2">
                <div class="icon">
                    <i class="flaticon-bag"></i>
                </div>

                <h3>Create Election</h3>

                <div class="contact-form">
                    <form id="contactForm" method="post" action="{{route('postElection')}}">
                        @csrf
                        @include('includes.message')
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="title" id="msg_subject"  class="form-control" required data-error="Please enter title" placeholder="Title">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" onfocus="(this.type='date')" name="start_date" id="name" class="form-control" required data-error="Please select " placeholder="Start Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" onfocus="(this.type='date')" name="end_date" id="email" class="form-control" required data-error="Please select date" placeholder="End Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" onfocus="(this.type='date')" name="nom_start_date" id="name" class="form-control" required data-error="Please select date" placeholder="Nomination Start Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" onfocus="(this.type='date')" name="nom_end_date" id="email" class="form-control" required data-error="Please select date" placeholder="Nomination End Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="description" class="form-control" id="message" cols="30" rows="6" required data-error="Write description" placeholder="Description"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="default-btn">SUBMIT <span></span></button>
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>

@endsection
