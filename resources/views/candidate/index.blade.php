@extends('layouts.app')

@section('content')
    <!-- Start Upcoming Events Area -->
    <section class="upcoming-events-area ptb-10 pb-70">
                <div class="single-upcoming-events-box">
                    <div class="events-box">
                        <div class="events-date">
                            <div class="date">
                                <div class="d-table">
                                    <div class="d-table-cell">
                                        <h6>Start date  {{$election->start_date}}</h6>
                                        <h6>End date  {{$election->end_date}}</h6>
                                        <div hidden>
                                            {{$date = strtotime($election->end_date)}}
                                            {{$remaining = $date - time()}}

                                            {{$days_remaining = floor($remaining / 86400)}}
                                            {{$hours_remaining = floor(($remaining % 86400) / 3600)}}
                                            {{$word="$days_remaining days and $hours_remaining hours left"}}
                                        </div>

                                        <p>{{$word}}</p>
                                        <i class="flaticon-calendar"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="events-content">
                            <div class="content">
                                <div class="row">
                                    <div class="col-sm-6">
                                <h3><a href="#">{{$election->title}}</a></h3>
                                <p>{{$election->description}}.</p>
                                <span class="location"><i class="fas fa-map-marker-alt"></i> 120 G-35 Spinsovila Sc, USA   </span>
                                <a href="{{route('election/leaders',$election->id)}}" class="join-now-btn">Elect Leaders</a><hr><br>
                                        <a class="btn btn-primary" href="{{route('candidate/apply',$election->id)}}">Apply</a>
                                        <a class="btn btn-warning" href="{{route('election/elect',$election->id)}}">Elect</a>
                                </div>
                                    <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="about-text">
                                                            <h3>Candidates</h3>
                                                            <p>Some text.</p>

                                                            <ul class="features-list">
                                                                @foreach($election->candidate as $key=>$candi)
                                                                    <li><a href="{{route('election/leader',$candi->user_id)}}"><i class="flaticon-right-arrow"></i></a> {{$candi->user->name}}--->{{$candi->posts->title}}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
<hr>

                                    </div>
                                </div>
                            </div>
                        </div>

    </section>
    <!-- End Upcoming Events Area -->



@endsection
