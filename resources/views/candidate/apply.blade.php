@extends('layouts.app')

@section('content')
    <div class="container">
        @if($cand>0)
        <div class="alert alert-danger">
            <button class="close" data-dismiss="alert">&times;</button>
            <h4>Warning!</h4>
            <p>Sorry you are   not ligible to apply in this election.</p>
            <p>They have  {{$cand}} under nomination.</p>
        </div>
            @endif
    </div>
    <!-- Start About Area -->
    <section class="about-area ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="about-image">
                        <img src="{{asset('images/user.png')}}" id="blah" alt="image">

                        <div class="text">
                            <span></span> {{$election->title}}
                        </div>

                        <div class="shape">
                            <img src="{{asset('assets/img/dot.png')}}" alt="image">
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="contact-form">
                        <form method="post" action="{{route('postApllication',$election->id)}}" enctype="multipart/form-data">
                            @csrf
                    <div class="about-content">
                        <span class="sub-title">{{Auth::user()->name}}</span><hr>
                        @include('includes.message')
                        <div class="form-group">
                            <label for="exampleInputEmail1">Picture</label>
                            <input type="file" name="avatars" class="form-control" onchange="readURL(this);" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter title">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" name="title" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter title">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Position</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="posts_id">
                                <option disabled="disabled" selected="selected">Select position</option>
                                @foreach($election->posts as $key=>$post)
                                <option value="{{$post->id}}">{{$post->title}}</option>
                               @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="comment">Portfolio:</label>
                            <textarea class="form-control" rows="5" id="comment" name="portfolio"></textarea>
                        </div>
                        @if($cand>0)

                           @else
                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="default-btn">SUBMIT APPLICATION <span></span></button>
                                {{--                                <div id="msgSubmit" class="h3 text-center hidden"></div>--}}
                                <div class="clearfix"></div>
                            </div>

                        @endif

                        <div class="signature-image">
                            <img src="{{asset('assets/img/signature-img.png')}}" alt="image">
                        </div>
                    </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>
    <!-- End About Area -->

@endsection
