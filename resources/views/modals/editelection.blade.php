<!-- Modal -->
<div class="modal fade" id="editelection" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Edit Election</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="contact-form">
                    <form id="contactForm" method="post" action="{{route('updateElection',$election->id)}}">
                        @csrf
                        <div class="row justify-content-center">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" value="{{$election->title}}" name="title" id="msg_subject"  class="form-control" required data-error="Please enter title" placeholder="Title">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" value="{{$election->start_date}}" onfocus="(this.type='date')" name="start_date" id="name" class="form-control" required data-error="Please select " placeholder="Start Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" value="{{$election->end_date}}" onfocus="(this.type='date')" name="end_date" id="email" class="form-control" required data-error="Please select date" placeholder="End Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" value="{{$election->nom_start_date}}" onfocus="(this.type='date')" name="nom_start_date" id="name" class="form-control" required data-error="Please select date" placeholder="Nomination Start Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="text" value="{{$election->nom_end_date}}" onfocus="(this.type='date')" name="nom_end_date" id="email" class="form-control" required data-error="Please select date" placeholder="Nomination End Date">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea name="description"  class="form-control" id="message" cols="30" rows="6" required data-error="Write description" placeholder="Description">{{$election->description}}</textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="default-btn">UPDATE ELECTION <span></span></button>
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
