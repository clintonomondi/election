@extends('layouts.app')

@section('content')

    <!-- Start Main Banner Area -->
    <div class="home-slides owl-carousel owl-theme">
        <div class="main-banner item-bg1 jarallax" data-jarallax='{"speed": 0.3}'>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="banner-content">
                            <h1>Help Bring The Change We Need</h1>
                            <p>It is enough that the people know there was an election. The people who cast the votes decide nothing. The people who count the votes decide everything.</p>

                            <a href="#" class="default-btn">Join With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner item-bg2 jarallax" data-jarallax='{"speed": 0.3}'>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="banner-content">
                            <h1>Help Bring The Change We Need</h1>
                            <p>It is enough that the people know there was an election. The people who cast the votes decide nothing. The people who count the votes decide everything.</p>

                            <a href="#" class="default-btn">Join With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="main-banner item-bg3 jarallax" data-jarallax='{"speed": 0.3}'>
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="container">
                        <div class="banner-content">
                            <h1>Help Bring The Change We Need</h1>
                            <p>It is enough that the people know there was an election. The people who cast the votes decide nothing. The people who count the votes decide everything.</p>

                            <a href="#" class="default-btn">Join With Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Main Banner Area -->



    <!-- Start Boxes Area -->
    <section class="boxes-area">
        <div class="container">
            <div class="row justify-content-center">
                @foreach($elections as $election)
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-box">
                        <div class="icon">
                            <i class="flaticon-tickets"></i>
                        </div>

                        <h3>{{$election->title}}</h3>

                        <p>{{$election->description}}.</p>
                        <h6>Start date  {{$election->start_date}}</h6>
                        <h6>End date  {{$election->end_date}}</h6>
                        <div hidden>
                            {{$date = strtotime($election->end_date)}}
                            {{$remaining = $date - time()}}

                            {{$days_remaining = floor($remaining / 86400)}}
                            {{$hours_remaining = floor(($remaining % 86400) / 3600)}}
                            {{$word="$days_remaining days and $hours_remaining hours left"}}
                        </div>

                        <p>{{$word}}</p>

                        <a href="{{route('election/info',$election->id)}}" class="details-btn"><i class="flaticon-right-arrow"></i></a>
                    </div>
                </div>
                  @endforeach

            </div>
        </div>
    </section>


    <!-- End Boxes Area -->
@endsection
