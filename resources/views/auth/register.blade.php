@extends('layouts.common')

@section('content')

    <div class="row justify-content-center">
        <div class="col-sm-6">
            <div class="single-box2">
                <div class="icon">
                    <i class="flaticon-login"></i>
                </div>

                <h3>Sign Up</h3>

                <div class="contact-form">
                    <form id="contactForm" method="post" action="{{route('register')}}">
                        @csrf
                        @include('includes.message')
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="name" id="msg_subject"  class="form-control" required data-error="Please Full Name" placeholder="Full Name">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="email" id="msg_subject"  class="form-control" required data-error="Please enter email" placeholder="Email">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="phone" id="msg_subject"  class="form-control" required data-error="Please enter Phone number" placeholder="Phone number">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="password"  name="password" id="name" class="form-control" required data-error="Please enetr passowrd " placeholder="Password">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <div class="form-group">
                                    <input type="password"  name="password_confirmation" id="name" class="form-control" required data-error="Please confirm password " placeholder="Password Confirmation">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>


                            <div class="col-lg-12 col-md-12">
                                <button type="submit" class="default-btn">SUBMIT <span></span></button>
                                {{--                                <div id="msgSubmit" class="h3 text-center hidden"></div>--}}
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>


            </div>
        </div>
    </div>

@endsection
