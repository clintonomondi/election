<!doctype html>
<html lang="en">

<!-- Mirrored from templates.envytheme.com/poxo/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 27 Mar 2020 05:39:37 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="{{asset('assets/js/jquery.min.js')}}"></script>
    <!-- Bootstrap Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Animate Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <!-- Font Awesome Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome.min.css')}}">
    <!-- Owl Carousel Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <!-- Odometer Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/odometer.min.css')}}">
    <!-- Magnific Popup Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
    <!-- FlatIcon CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
    <!-- MeanMenu CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/meanmenu.css')}}">
    <!-- Swiper Min CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/swiper.min.css')}}">
    <!-- Style CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/elect.css')}}">

    <title>Poxo Election</title>

    <link rel="icon" type="image/png" href="{{asset('assets/img/favicon.png')}}">

    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>

    <script src="{{asset('alert/alertify.min.js')}}"></script>
    <link rel="stylesheet" href="{{asset('alert/css/alertify.min.css')}}" />
    <link rel="stylesheet" href="{{asset('alert/css/themes/default.min.css')}}" />

</head>

<body>

<!-- Preloader -->
<div class="preloader">
    <div class="loader">
        <div class="shadow"></div>
        <div class="box"></div>
    </div>
</div>
<!-- End Preloader -->

<!-- Start Header Area -->
<header class="header-area">

    <!-- Start Top Header -->
    <div class="top-header">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-12">
                    <div class="city-temperature">
                        <i class="fas fa-cloud-sun"></i>
                        <span>28.5<sup>c</sup></span>
                        <b>Dubai</b>
                    </div>

                    <ul class="top-nav">
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Forums</a></li>
                        <li><a href="#">Contact</a></li>
                        <li><a href="#">Advertisement</a></li>
                    </ul>
                </div>

                <div class="col-lg-6 col-md-12 text-right">
                    <ul class="top-social">
                        <li><span>{{Auth::user()->name}}</span></li>
                        <li><a href="#" target="_blank"><i class="fa fa-user"></i></a></li>

                    </ul>

                    <div class="header-date">
                       <a onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();"> <i class="far fa-clock"></i>Logout</a>
                    </div>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End Top Header -->

    <!-- Start Navbar Area -->
    <div class="navbar-area">
        <div class="poxo-responsive-nav">
            <div class="container">
                <div class="poxo-responsive-menu">
                    <div class="logo">
                        <a href="{{route('home')}}">
                            <img src="{{asset('assets/img/logo.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="poxo-nav">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="{{route('home')}}">
                        <img src="{{asset('assets/img/logo.png')}}" alt="logo">
                    </a>

                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a href="{{route('home')}}" class="nav-link active">Home</a></li>

                            <li class="nav-item"><a href="about.html" class="nav-link">About Us</a></li>

                            <li class="nav-item"><a href="#" class="nav-link">Election <i class="fas fa-chevron-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li class="nav-item"><a  href="{{route('election')}}" class="nav-link">Add Election</a></li>

                                    <li class="nav-item"><a  href="{{route('elections')}}" class="nav-link">Elections</a></li>
                                </ul>
                            </li>

                            <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>
                        </ul>

                        <div class="others-option">
                            <div class="option-item"><i class="search-btn flaticon-search"></i>
                                <i class="close-btn flaticon-cross-out"></i>

                                <div class="search-overlay search-popup">
                                    <div class='search-box'>
                                        <form class="search-form">
                                            <input class="search-input" name="search" placeholder="Search" type="text">

                                            <button class="search-button" type="submit"><i class="fas fa-search"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="call-us">
                                <div class="icon">
                                    <i class="flaticon-phone-call"></i>
                                </div>
                                Call Us:
                                <a href="#">+1 518 285679</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Navbar Area -->

</header>
<!-- End Header Area -->

<main class="py-4">
    @yield('content')
</main>



<div class="go-top"><i class="fas fa-chevron-up"></i><i class="fas fa-chevron-up"></i></div>

<!-- jQuery Min JS -->

<!-- Popper Min JS -->
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<!-- Bootstrap Min JS -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- Owl Carousel Min JS -->
<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
<!-- Magnific Popup Min JS -->
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Appear Min JS -->
<script src="{{asset('assets/js/jquery.appear.min.js')}}"></script>
<!-- Odometer Min JS -->
<script src="{{asset('assets/js/odometer.min.js')}}"></script>
<!-- Swiper Min JS -->
<script src="{{asset('assets/js/swiper.min.js')}}"></script>
<!-- Parallax Min JS -->
<script src="{{asset('assets/js/parallax.min.js')}}"></script>
<!-- MeanMenu JS -->
<script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
<!-- WOW Min JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Form Validator Min JS -->
<script src="{{asset('assets/js/form-validator.min.js')}}"></script>
<!-- Contact Form Min JS -->
<script src="{{asset('assets/js/contact-form-script.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('assets/js/main.js')}}"></script>
<script src="{{asset('assets/js/common.js')}}"></script>
<script src="{{asset('js/api.js')}}"></script>
<script src="{{asset('js/elect.js')}}"></script>

<link rel="stylesheet" href="{{asset('loader/src/css/preloader.css')}}">
<script src="{{asset('loader/src/js/jquery.preloader.min.js')}}"></script>

<script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'comment' );
</script>
</body>

<!-- Mirrored from templates.envytheme.com/poxo/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 27 Mar 2020 05:40:12 GMT -->
</html>
