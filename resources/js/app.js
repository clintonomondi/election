import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueLoading from 'vuejs-loading-plugin'
import Notifications from 'vue-notification'


Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueLoading);
Vue.use(Notifications);


import App from './view/App'
import  Home from './view/Home'
import  Election from './election/add'


const router = new VueRouter({
    mode: 'history',
    routes: [

        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/addelection',
            name: 'addelection',
            component: Election,
        },


    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
