<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    protected  $fillable=['title','description','nom_end_date','nom_start_date','start_date','end_date','status','created_by','updated_by'];

    public  function posts(){
        return $this->hasMany(Posts::class);
    }

    public  function candidate(){
        return $this->hasMany(Candidate::class);
    }
}
