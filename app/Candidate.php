<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected  $fillable=['user_id','election_id','posts_id','status','avatar','portfolio','title'];

    public  function  election(){
        return $this->belongsToMany(Election::class);
    }

    public  function  user(){
        return $this->belongsTo(User::class);
    }

    public  function posts(){
        return $this->belongsTo(Posts::class);
    }
}
