<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elected extends Model
{
    protected  $fillable=['user_id','election_id','posts_id','candidate_id'];
}
