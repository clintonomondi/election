<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected  $fillable=['title','description','election_id'];

    public  function election(){
        return $this->belongsTo(Election::class);
    }

    public  function candidate(){
        return $this->hasMany(Candidate::class);
    }
}
