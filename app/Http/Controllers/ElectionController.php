<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Elected;
use App\Election;
use App\Posts;
use App\Temp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ElectionController extends Controller
{
    public  function index(){
        return view('election.add');
    }

    public  function postElection(Request $request){

        $request['created_by']=Auth::user()->id;
        $data=Election::create($request->all());
        return redirect()->back()->with('success','New election created successfully');
    }

    public  function elections(){
        $elections=Election::all();

        return view('election.elections',compact('elections'));
    }

    public  function edit($id){
        $election=Election::findorFail($id);
        return view('election.edit',compact('election'));
    }

    public  function updateStatus(Request $request){
        $data=Election::findorFail($request->id);
        $data->update($request->all());
        return ['status'=>true,'message'=>'Status change successfully'];
    }

    public  function savePosts(Request $request,$id){
        $count = count($request->title);
        for($i = 0; $i < $count; ++$i) {
            if(empty($request->title[$i]) || empty($request->description[$i])){

            }
            $data=array(
                'election_id'=>$id,
                'title'=>$request->title[$i],
                'description'=>$request->description[$i],
            );
            Posts::insert($data);
        }

        return redirect()->back()->with('success','Post added successfully');
    }

    public  function updateElection(Request $request,$id){
        $data=Election::findorFail($id);
        $data->update($request->all());
        return redirect()->back()->with('success','Election updated   successfully');
    }

    public  function info($id){

        $election=Election::find($id);
        return view('election.info',compact('election'));
    }

    public  function leaders($id){

        $election=Election::find($id);
        return view('election.leaders',compact('election'));
    }

    public  function leader($id){
        $candidate=Candidate::where('user_id',$id)->first();
        return view('election.leader',compact('candidate'));
    }

    public  function elect($id){
        $election=Election::find($id);
        $data=Elected::where('user_id',Auth::user()->id)->where('election_id',$id)->count();
        return view('election.elect',compact('election','data'));
    }

    public  function postTemp(Request $request){

        $datas = DB::select( DB::raw("INSERT INTO temps(user_id,election_id,posts_id,candidate_id)VALUES('$request->user_id',
      '$request->election_id','$request->posts_id','$request->candidate_id')
      ON DUPLICATE KEY UPDATE candidate_id='$request->candidate_id'") );


        $data=Temp::where('user_id',Auth::user()->id)->where('election_id',$request->election_id)->get();
        return ['status'=>true,'message'=>'success','data'=>$data];
    }

    public  function postElected(Request $request,$id){
        $col=Temp::where('user_id',Auth::user()->id)->where('election_id',$id)->get();
        if(empty($col)){
            return redirect()->back()->with('error','You have not elected any candidate for this election');
        }

        foreach($col as $data) {
            $datas=array(
                'election_id'=>$data->election_id,
                'user_id'=>$data->user_id,
                'posts_id'=>$data->posts_id,
                'candidate_id'=>$data->candidate_id,
            );
            $elect=Elected::insert($datas);
        }

        return redirect()->back()->with('success','Election submitted successfully');

    }
}
