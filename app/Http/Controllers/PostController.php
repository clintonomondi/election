<?php

namespace App\Http\Controllers;

use App\Election;
use App\Posts;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public  function index($id){
        $election=Election::find($id);
        return view('posts.index',compact('election'));
    }

    public  function leaders($id){
        $posts=Posts::findorFail($id);
        return view('posts.leaders',compact('posts'));
    }
}
