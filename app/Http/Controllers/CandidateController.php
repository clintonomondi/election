<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Election;
use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CandidateController extends Controller
{

    public  function index($id){
        $election=Election::find($id);
        return view('candidate.index',compact('election'));

    }

    public  function apply($id){
        $election=Election::find($id);
        $cand=Candidate::where('user_id',Auth::user()->id)->where('election_id',$id)->count();
        return view('candidate.apply',compact('election','cand'));
    }

    public  function postApllication(Request $request,$id){
        $validatedData = $request->validate([
            'avatars' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);

        $fileNameWithExt=$request->file('avatars')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('avatars')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('avatars')->storeAs('/public/avatars',$fileNameToStore);
        $request['avatar']=$fileNameToStore;
        $request['user_id']=Auth::user()->id;
        $request['election_id']=$id;
        $data=Candidate::create($request->all());

        return redirect()->back()->with('success','Application made successfully');
    }
}
